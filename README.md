# My Queue

- a simple queue that is shared between two threads.
- uses the producer-consumer design pattern for data exchange through the queue
- implements a _sentinel = object() to terminate queue communication - thus terminating producer-consumer.

# Getting Started

__setup__

- docker installation (this is for Ubuntu 18.04)
```bash
make docker
```

- setup pre-hook for git commit with flake8 linting
```bash
make pre-hook
```

- spin up the docker containers
```bash
make start
```

__more__
- look at the Makefile and run commands