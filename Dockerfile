FROM python:3.6
LABEL maintainer="Matthew McCann <matmccann@gmail.ca>"

WORKDIR /my_queue

RUN apt-get -y update
RUN apt-get -y upgrade

RUN apt-get install -y \
    gstreamer1.0-tools \
    libgstreamer1.0-dev \
    gstreamer1.0-plugins-\* \
    gstreamer1.0-libav \
    libgstrtspserver-1.0-0 \
    libgstrtspserver-1.0-dev

# Setup GDAL
RUN apt-get install -y software-properties-common ffmpeg libgdal-dev
RUN echo "CPLUS_INCLUDE_PATH=/usr/include/gdal" >> /root/.bashrc
RUN echo "C_INCLUDE_PATH=/usr/include/gdal" >> /root/.bashrc
RUN echo "deb     http://ppa.launchpad.net/ubuntugis/ppa/ubuntu buster main" >> /etc/apt/sources.list
RUN echo "deb-src http://ppa.launchpad.net/ubuntugis/ppa/ubuntu buster main" >> /etc/apt/sources.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 314DF160
RUN apt-add-repository ppa:ubuntugis/ppa
# apt-get update will fail! No GPG key for ubuntugis

# Python module path for the root import location
# same as 'environment': PYTHONPATH='/my_queue' in docker-compose.yml
ENV PYTHONPATH /my_queue


## Install python libraries
COPY requirements.txt /my_queue/requirements.txt
RUN pip install -r /my_queue/requirements.txt
