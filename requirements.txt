# localization dependencies
pip==20.1
setuptools==46.1.3
flake8==3.7.9
GDAL==2.4.2
numpy==1.18.2
opencv-python==4.2.0.34
homography==0.1.5
