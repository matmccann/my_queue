# my_queue/__init__.py
import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
# Object that signals shutdown
_sentinel = object()
