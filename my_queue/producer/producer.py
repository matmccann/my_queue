# my_queue/producer/producer.py
import logging
from my_queue.data.dummy_data import dummy_data


# A thread that produces data
def producer(out_q, _sentinel):

    while True:
        # Produce some data
        for key, value in dummy_data.items():
            data = (key, value)
            out_q.put(data)
            logging.debug(f"PRODUCER: \t{data}")
        out_q.put(_sentinel)
        break
