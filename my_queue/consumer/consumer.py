# my_queue/consumer/consumer.py

import logging


# A thread that consumes data
def consumer(in_q, _sentinel):
    for data in iter(in_q.get, _sentinel):
        logging.debug(f"CONSUMER:\t{data}")
        # Process the data
        pass
