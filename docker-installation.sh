#! /bin/sh

# EXAMPLE OF OF RUNNING THIS SCRIPT
# $./docker-installation.sh "your-username"

username=$1

sudo sh -c '(
printf "*****\n\nDocker installation for Ubuntu 18.04*****\n\n"
apt-get remove docker docker-engine docker.io containerd runc
apt-get update

printf "\n\n*****Starting installation *****\n\n"
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

printf "\n\n*****Docker GPG key*****\n\n"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

printf "\n\n*****Download Docker*****\n\n"
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

printf "\n\n*****Download Docker*****\n\n"
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io

printf "\n\n*****Download docker-compose*****\n\n"
curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
printf "\nVersion:\t"
docker-compose --version

printf "\n\n*****Verify Download Success with docker run hello-world*****\n\n"
docker run hello-world
)'

printf "\n\n*****Docker User Configuration*****\n\n"
# add user if variable is passed in
if [ -n "$username" ]
then
    printf "Setting Docker Username >>  %s*****\n" "$username"
    sudo usermod -aG docker "$username"
else
     printf "*****Did Not Configure Docker Username*****\n"
fi