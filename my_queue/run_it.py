# my_queue/run_it.py
import logging
import os
from pathlib import Path
from queue import Queue
from threading import Thread
from my_queue import _sentinel
from my_queue.consumer.consumer import consumer
from my_queue.producer.producer import producer


def setup_logging(folder='/my_queue/my_queue/logs'):
    try:
        filepath = folder + "/python.log"
        Path(folder).touch(mode=0o777, exist_ok=True)
        os.system(f"ln -sf /dev/stdout {filepath}")
        logging.basicConfig(filename=filepath, filemode='w', level=logging.DEBUG)
        logging.debug(f"\t\tLOG FILE LOCATION: {filepath}")
        pass
    except Exception as err:
        print(f"ERROR (setup_logging): {err}")


if __name__ == "__main__":

    # Configure module wide logging
    setup_logging()

    # Create the shared queue and launch both threads
    q = Queue()
    t1 = Thread(target=consumer, args=(q, _sentinel))
    t2 = Thread(target=producer, args=(q, _sentinel))
    logging.debug("\t\tStarting threads ...")
    t1.start()
    t2.start()
    logging.debug("\t\tMAIN LOOP FINISHED")
