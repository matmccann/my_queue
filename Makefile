# INITIAL SET UP
docker:
ifndef user
	@echo "try this: make docker user='your-name'"
else
	./docker-installation.sh $(user)
endif

docker-login:
	@echo "username: <email for gitlab.com>   password:<password to gitlab.com>"
	docker login registry.gitlab.com

pre-hook:
	touch .git/hooks/pre-commit && chmod 777 .git/hooks/pre-commit
	echo "#!/bin/sh" >> .git/hooks/pre-commit
	echo "flake8 ." >> .git/hooks/pre-commit

# RUNNING JOBS
start:
	docker-compose up -d --build
threading:
	docker exec -it my_queue bash -c "python3 my_queue/run_it.py"
ssh:
	docker exec -it my_queue bash
test:
	docker exec -it my_queue bash -c "python3 -m unittest -vvv"
logs:
	docker logs my_queue

# DOCKER CLEANUP
stop:
	docker-compose down
	sudo rm -rf queue/logs
stop-clean:
	@echo "Stop and prune"
	docker-compose down --rmi all --volumes --remove-orphans
	docker image prune --filter label=stage=ui-builder --force
clean:
	@ echo 'RUN: pruning containers, volumes, and networks'
	docker system prune -a && docker volume prune && docker network prune
	@echo "\n\nChecking docker containers"
	docker ps

# UPDATE GITLAB CONTAINER REGISTRY
image-build-push: docker-login
	docker build -t registry.gitlab.com/matmccann/my_queue .
	docker push registry.gitlab.com/matmccann/my_queue
